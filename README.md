Angular-Pseudo-Cypher
=====================

A demonstration of Angular again (also using [jade](http://jade-lang.com/), [coffee](http://coffeescript.org/), [bootstrap](http://getbootstrap.com/) and a little [underscore.js](http://underscorejs.org/)).

Demo
----

<http://www.cip.ifi.lmu.de/~bruder/angular-pseudo-cypher/>

Usage
-----

    make

    (open index.html)
    
    (write something)