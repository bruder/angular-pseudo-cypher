angular
  .module('CypherApp', [])
  .controller('CypherCtrl', ($scope) ->
    $scope.input = 'Hello angular filter. This is live updating.'
  )
  .filter('cypher', ->
    return (input) ->
      forward = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ .!?".split ''
      reverse = "zyxwvutsrqponmlkjihgfedcbaZYXWVUTSRQPONMLKJIHGFEDCBA?!. ".split ''
      convert = _.object forward, reverse      # http://underscorejs.org/#object
      output = ''
      for char in input.split ''
        output += convert[char]
      output # return
  )
  