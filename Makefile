# by defualt, pick the first rule
default: compile

# always build these files
# .PHONY: index.html script.js
compile: index.html

# index.html has script.js as a prerequisite
index.html: script.js

# pattern rule: how to produce html from jade
%.html: %.jade
	jade -P index.jade &>/dev/null

# pattern rule: how to produce js from coffee
%.js: %.coffee
	coffee -c script.coffee

# consider target 'clean' as being always out-of-date
.PHONY: clean
clean:
	rm -rf *.js *.html

